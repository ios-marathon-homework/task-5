import UIKit
import Foundation

//Task 1-2
class ClassStudent {
    var name : String
    var surname : String
    var avgScore : Double
    var birthYear : Int
    
    init(name: String, surname: String, birthYear: Int, avgScore: Double) {
        self.name = name
        self.surname = surname
        self.birthYear = birthYear
        self.avgScore = avgScore
    }
}

struct StructStudent {
    var name : String
    var surname : String
    var birthYear : Int
    var avgScore : Double
}

var classStudent1 = ClassStudent(name: "Alex", surname: "Alexandrov", birthYear: 2000, avgScore: 3.3)
var classStudent2 = ClassStudent(name: "Ivan", surname: "Ivanov", birthYear: 1999, avgScore: 4)
var classStudent3 = ClassStudent(name: "Boris", surname: "Borisov", birthYear: 2001, avgScore: 3.7)
var classStudent4 = ClassStudent(name: "Zack", surname: "Zackov", birthYear: 1998, avgScore: 4.1)

var structStudent1 = StructStudent(name: "Alex", surname: "Alexandrov", birthYear: 2000, avgScore: 3.3)
var structStudent2 = StructStudent(name: "Ivan", surname: "Ivanov", birthYear: 1999, avgScore: 4)
var structStudent3 = StructStudent(name: "Boris", surname: "Borisov", birthYear: 2001, avgScore: 3.7)
var structStudent4 = StructStudent(name: "Zack", surname: "Zackov", birthYear: 1998, avgScore: 4.1)

var classGradeBook = [classStudent1, classStudent2, classStudent3, classStudent4]
var structGradeBook = [structStudent1, structStudent2, structStudent3, structStudent4]

print("ЖУРНАЛ СТУДЕНТОВ")
print("----------------")

func printClassGradebook(students:[ClassStudent]) {
    print("#\tФамилия\tИмя\tГод.Рождения\tСр.Балл")
    var n = 0
    for i in 0..<students.count {
        n += 1
        print("\(n)\t\(students[i].surname)\t\(students[i].name)\t\(students[i].birthYear)\t\(students[i].avgScore)")
    }
}

func printStructGradebook(students:[StructStudent]) {
    print("#\tФамилия\tИмя\tГод.Рождения\tСр.Балл")
    var n = 0
    for i in 0..<students.count {
        n += 1
        print("\(n)\t\(students[i].surname)\t\(students[i].name)\t\(students[i].birthYear)\t\(students[i].avgScore)")
    }
}

printClassGradebook(students: classGradeBook)

print()

printStructGradebook(students: structGradeBook)

print()

//Task 3 sort by avgScore

print("Sort by avgScore class")
classGradeBook.sort {$0.avgScore > $1.avgScore}
printClassGradebook(students: classGradeBook)

print()

print("Sort by avgScore struct")
structGradeBook.sort {$0.avgScore > $1.avgScore}
printStructGradebook(students: structGradeBook)

//Task 4 sort by name

func sortByNameSurName(first: StructStudent, second: StructStudent) -> Bool{
    if first.surname != second.surname{
        return (first.surname < second.surname)
    }
    else{
        return (first.name < second.name)
    }
}

func sortByNameSurName(first: ClassStudent, second: ClassStudent) -> Bool{
    if first.surname != second.surname{
        return (first.surname < second.surname)
    }
    else{
        return (first.name < second.name)
    }
}


print("Task 4 sort by name")

print("Sort by name class")
classGradeBook.sort(by: sortByNameSurName(first:second:))
printClassGradebook(students: classGradeBook)

print()

print("Sort by name struct")
structGradeBook.sort(by: sortByNameSurName(first:second:))
printStructGradebook(students: structGradeBook)





